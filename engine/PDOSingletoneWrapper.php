<?php

class PDOSingletoneWrapper {

	private static $_instance;

	private $_db;
	
	private function __construct()
	{
		$dsn = sprintf("%s:dbname=%s;host=%s", DB_DRIVER, DB_NAME, DB_HOST);
		$this->db = new PDO($dsn, DB_USER, DB_PASSWORD);
	}

	public static function getInstance()
	{
		if (self::$_instance === null) {
			self::$_instance = new PDOSingletoneWrapper();
		}

		return self::$_instance;
	}

	public function getDB()
	{
		return $this->db;
	}

}