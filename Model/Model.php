<?php

namespace test2\Model;

abstract class Model {

	/** @var SingletonePDOWrapper инстанс db */
	protected $_db;

	/** Конструктор - получет инстанс db */
	public function  __construct() {
		$this->_db = \PDOSingletoneWrapper::getInstance()->getDB();
	}
}