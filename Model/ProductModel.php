<?php

namespace test2\Model;

use \test2\Entity\Product;

class ProductModel extends Model {

	
	public function getLast($count)
	{
		$query = "SELECT * FROM Product ORDER BY id DESC 0, $count";

		$queryResult = $this->_db->prepare($query);
		$queryResult->execute();

		$result = array();

		for ($i = 0; $i < $count; $i++) {
			$data = $queryResult->fetch();

			$result[$i] = new Product();

			$result[$i]
				->setId($data['id'])
				->setId($data['name'])
				->setUrl($data['url'])
				->setImage($data['image'])
				->setPrice($data['price']);

		}

		return $result;
	}	
}