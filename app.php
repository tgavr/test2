<?php

require_once "engine/PDOSingletoneWrapper.php";
require_once "engine/config.inc";

spl_autoload_register(function($class) {

	if (preg_match("/^test2/", $class)) {
		$class = str_replace("test2\\", "", $class);
		$file = str_replace("\\", "/", $class) . ".php";

		require_once $file;
	}
});

use test2\Model\ProductModel;
use test2\Entity\Product;

$model = new ProductModel();

$result = $model->getLast(100);

$rendered = ""; $renderedCounter = 0;
$forFileCached = array();

function renderProduct(Product $product) {
	return sprintf(
		"<td>id:%s<br>name:%s<br>url:<a href='%s'>%s</a><br><img src='%s'></td>",
		$product->getId(),
		$product->getName(),
		$product->getUrl(),
		$product->getUrl(),
		$product->getImage()
	);
}

foreach ($result as $product) {

	if ($product->hasImage()) {
		if ($renderedCounter === 0) {
			$rendered .= "<tr>";
		}
		$renderedCounter++;
		$rendered .= renderProduct($product);
		if ($rendered === 3) {
			$renderedCounter = 0;
			$rendered .= "</tr>";
		}
	} else {
		$forFileCached[] = $product->getId();
	}
}

file_put_contents("noimage.txt", implode(", ", $forFileCached));

echo "<table>$rendered</table>";